import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListVehiclesComponent } from './vehicles/list-vehicles/list-vehicles.component';

const routes: Routes = [
  {component: ListVehiclesComponent, path: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
