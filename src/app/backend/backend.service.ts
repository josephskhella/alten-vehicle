import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private _http: HttpClient,
    private db: AngularFireDatabase) { }

  updateVehicleState(id, state): Promise<any> {
    return this.db.database.ref(`/vehicles/${id}`).update({state: state});
  }
}