import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackendComponent } from './backend/backend.component';
import { BackendService } from './backend.service';

@NgModule({
  declarations: [BackendComponent],
  imports: [
    CommonModule
  ],
  providers: [BackendService]
})
export class BackendModule { }
