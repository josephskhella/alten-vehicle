import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend/backend.service';

@Component({
  selector: 'alten-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  constructor(private backendService: BackendService) { }

  ngOnInit() {
    setInterval(() => {
      let randomId = Math.floor(Math.random() * 10);
      let randomState = Math.floor(Math.random() * 2) === 0 ? true : false;

      this.backendService.updateVehicleState(randomId, randomState).then()
    }, 2000)
  }

}
