import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'alten-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
@Output() toggleMenu:EventEmitter<any> = new EventEmitter<any>();

fullscreen;

  constructor(
    private translate: TranslateService
  ) { }
  

  ngOnInit() {
  }


  changeLang(lang){
    this.translate.use(lang)
  }
  

  fireToggleMenu() {
    this.toggleMenu.emit();
  }

  toggleFullScreen() {
    if (this.fullscreen) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document['mozCancelFullScreen']) {
        /* Firefox */
        document['mozCancelFullScreen']();
      } else if (document['webkitExitFullscreen']) {
        /* Chrome, Safari and Opera */
        document['webkitExitFullscreen']();
      } else if (document['msExitFullscreen']) {
        /* IE/Edge */
        document['msExitFullscreen']();
      }
    } else {
      let elem = document.documentElement;
      let methodToBeInvoked = elem.requestFullscreen ||
        elem['webkitRequestFullScreen'] || elem['mozRequestFullscreen']
        ||
        elem['msRequestFullscreen'];
      if (methodToBeInvoked) methodToBeInvoked.call(elem);
    }
    this.fullscreen = !this.fullscreen;
  }

}
