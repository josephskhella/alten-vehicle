import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { LayoutModule } from '../layout.module';
import { VehiclesModule } from 'src/app/vehicles/vehicles.module';
import { BackendModule } from 'src/app/backend/backend.module';
import { MaterialModule } from 'src/app/material.module';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { VehiclesEffects } from 'src/app/store/effects/vehicle.effects';
import { appReducers } from 'src/app/store/reducers/app.reducers';
import { CustomersEffects } from 'src/app/store/effects/customer.effects';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { HttpLoaderFactory } from 'src/app/app.component.spec';
import { VehicleService } from 'src/app/services/vehicle.service';
import { CustomerService } from 'src/app/services/customer.service';
import { protractor, browser, element, by } from 'protractor';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        LayoutModule,
        VehiclesModule,
        BackendModule,
        MaterialModule,
        HttpClientModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot([VehiclesEffects, CustomersEffects]),
        StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      providers: [VehicleService, CustomerService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should translate', () => {

    // let button = fixture.debugElement.nativeElement.querySelector('.language-btn');
    // button.click();
    // browser.wait(protractor.ExpectedConditions.elementToBeClickable(element(by.css('.sw-btn')))
    //   , 5000, 'Element taking too long to appear in the DOM');
    // fixture.detectChanges();
    // button = fixture.debugElement.nativeElement.querySelector('.sw-btn');
    // button.click();
    // fixture.detectChanges();
    
    // let headerMsg = fixture.debugElement.nativeElement.querySelector('.swwelcome-msg');

    // expect(headerMsg.textContent).toContain('Välkommen till Alten Fordonsövervakning');
  });


});
