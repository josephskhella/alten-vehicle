import { ICustomer } from './customer.model';

export interface IVehicle {
    id: string;
    regNr: string;
    state: boolean;
    customer: ICustomer;
}