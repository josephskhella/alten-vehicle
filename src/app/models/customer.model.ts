export interface ICustomer {
    id: string;
    name: string;
    address: string;
}