import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVehiclesComponent } from './list-vehicles.component';
import { BackendModule } from 'src/app/backend/backend.module';
import { VehiclesModule } from '../vehicles.module';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from 'src/app/material.module';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { appReducers } from 'src/app/store/reducers/app.reducers';
import { EffectsModule } from '@ngrx/effects';
import { VehiclesEffects } from 'src/app/store/effects/vehicle.effects';
import { CustomersEffects } from 'src/app/store/effects/customer.effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { HttpLoaderFactory } from 'src/app/app.component.spec';
import { CustomerService } from 'src/app/services/customer.service';
import { VehicleService } from 'src/app/services/vehicle.service';

describe('ListVehiclesComponent', () => {
  let component: ListVehiclesComponent;
  let fixture: ComponentFixture<ListVehiclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        LayoutModule,
        VehiclesModule,
        BackendModule,
        MaterialModule,
        HttpClientModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot([VehiclesEffects, CustomersEffects]),
        StoreRouterConnectingModule.forRoot({stateKey: 'router'}),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
      ],
      providers: [VehicleService, CustomerService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should get changed row', () => {
    let oldVehicles = [ {
      "customer" : 0,
      "id" : "y67r675",
      "regNr" : "8778tbt876",
      "state" : true
    }, {
      "customer" : 1,
      "id" : "67g67r56",
      "regNr" : "7h6767r56r",
      "state" : true
    }];
    let newVehicles = [ {
      "customer" : 0,
      "id" : "y67r675",
      "regNr" : "8778tbt876",
      "state" : true
    }, {
      "customer" : 1,
      "id" : "67g67r56",
      "regNr" : "7h6767r56r",
      "state" : false
    }];
    let changedRow = component.getChangedRow(oldVehicles, newVehicles);

    expect(changedRow.id).toBe("67g67r56");
  });
});
