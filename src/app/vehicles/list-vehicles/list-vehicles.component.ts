import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { GetVehicles } from 'src/app/store/actions/vehicle.actions';
import { selectVehicleList } from 'src/app/store/selectors/vehicle.selectors';
import { select, Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { IVehicle } from 'src/app/models/vehicle.model';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'alten-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.scss']
})
export class ListVehiclesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'regNr', 'state', 'customer.name', 'customer.address'];
  dataSource: MatTableDataSource<IVehicle>;

  vehicles$ = this._store.pipe(select(selectVehicleList));
  vehiclesData;

  prevVehicles;
  changedVehicle;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private translate: TranslateService,
    private _store: Store<IAppState>
  ) {
    this.vehicles$.subscribe(data => {
      if (data) {
        this.prevVehicles = this.vehiclesData;
        this.vehiclesData = data;
        this.changedVehicle = this.getChangedRow(this.prevVehicles, this.vehiclesData);
        if (!this.dataSource) {
          this.dataSource = new MatTableDataSource(this.vehiclesData);
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }, 1000);
        } else {
          if (this.changedVehicle) {
            let row = this.dataSource.data.find(d => d.id === this.changedVehicle.id);
            row.state = this.changedVehicle.state;
            row['changed'] = true;
            setTimeout(() => {
              row['changed'] = false;
            }, 1000);
          }
        }
      }
    })
  }

  getChangedRow(prevVehicles, newVehicles) {
    if (prevVehicles && newVehicles) {
      for (let i = 0; i < prevVehicles.length; i++) {
        if (prevVehicles[i].state !== newVehicles[i].state) {
          return newVehicles[i];
        }
      }
    }
  }

  ngOnInit() {
    this._store.dispatch(new GetVehicles());
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
console.log(this.dataSource);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
