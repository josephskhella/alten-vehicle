import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListVehiclesComponent } from './list-vehicles/list-vehicles.component';
import { MaterialModule } from '../material.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ListVehiclesComponent],
  imports: [
    CommonModule,
    MaterialModule,
    TranslateModule
  ]
})
export class VehiclesModule { }
