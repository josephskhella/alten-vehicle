import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { IVehicleHttp } from '../models/http-models/vehicle-http.model';
import { IVehicle } from '../models/vehicle.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';
import { map } from 'rxjs/operators'
import { FirebaseDatabase } from '@angular/fire';

@Injectable()
export class VehicleService {
  vehiclesUrl = `${environment.apiUrl}vehicles.json`;

  constructor(private _http: HttpClient,
    private firebaseService: AngularFirestore,
    private db: AngularFireDatabase) { }

  getVehicles() {
    return this.db.list('/vehicles').snapshotChanges();
  //   return new Promise<any>((resolve, reject) => {
  //     this.firebaseService.collection('/vechiles').snapshotChanges()
  //     .subscribe(snapshots => {
  //       resolve(snapshots);
  //     });
  // })
}



}