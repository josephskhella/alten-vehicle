import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { ICustomer } from '../models/customer.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class CustomerService {
  customersUrl = `${environment.apiUrl}customers.json`;

  constructor(private _http: HttpClient,
    private firebaseService: AngularFirestore,
    private db: AngularFireDatabase) { }

  getCustomers() {
    return new Promise<any>((resolve, reject) => {
      this.firebaseService.collection('/customers').snapshotChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
        });
    })
  }

  getCustomer(id) {
    return this.db.object(`/customers/${id}`).snapshotChanges();
  }
}