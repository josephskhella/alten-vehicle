import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store, select } from '@ngrx/store';
import { selectVehicleList } from './store/selectors/vehicle.selectors';
import { GetVehicles } from './store/actions/vehicle.actions';
import { IAppState } from './store/state/app.state';
import { selectCustomerList } from './store/selectors/customer.selectors';
import { GetCustomers } from './store/actions/customer.actions';
import { AngularFirestore } from '@angular/fire/firestore';
import { ICustomer } from './models/customer.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'alten-vehicle';
  customers$ = this._store.pipe(select(selectCustomerList));
  
  constructor(private translate: TranslateService,
    private _store: Store<IAppState>) {
    translate.addLangs(['en', 'sw']);
    translate.setDefaultLang('sw');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|sw/) ? browserLang : 'en');


  }

  ngOnInit() {
    // this._store.dispatch(new GetCustomers());
  }

}
