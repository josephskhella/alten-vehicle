import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { IAppState } from '../state/app.state';
import { Store, select } from '@ngrx/store';
import { map, withLatestFrom, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import { ICustomer } from 'src/app/models/customer.model';
import { GetCustomers, GetCustomersSuccess, GetCustomerSuccess, ECustomerActions, GetCustomer } from '../actions/customer.actions';
import { selectCustomerList } from '../selectors/customer.selectors';

@Injectable()
export class CustomersEffects {
    @Effect()
    getCustomer$ = this._actions$.pipe(
        ofType<GetCustomer>(ECustomerActions.GetCustomer),
        map(action => action.payload),
        withLatestFrom(this._store.pipe(select(selectCustomerList))),
        switchMap(([id, customers]) => {
            const selectedCustomer = customers.filter(customer => customer.id === id)[0];
            return of(new GetCustomerSuccess(selectedCustomer));
        })
    );

    @Effect()
    getCustomers$ = this._actions$.pipe(
        ofType<GetCustomers>(ECustomerActions.GetCustomers),
        switchMap(() => this._customerService.getCustomers()),
        switchMap((customers: ICustomer[]) => of(new GetCustomersSuccess(customers.map(e => {
            return {
              id: e['payload']['doc'].id,
              ...e['payload']['doc'].data()
            } as ICustomer;
          }))))
    );

    constructor(
        private _customerService: CustomerService,
        private _actions$: Actions,
        private _store: Store<IAppState>
    ){}
}