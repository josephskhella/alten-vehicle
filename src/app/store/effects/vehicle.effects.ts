import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { IAppState } from '../state/app.state';
import { Store, select } from '@ngrx/store';
import { GetVehicle, EVehicleActions, GetVehicleSuccess, GetVehicles, GetVehiclesSuccess } from '../actions/vehicle.actions';
import { map, withLatestFrom, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { selectVehicleList } from '../selectors/vehicle.selectors';
import { IVehicleHttp } from 'src/app/models/http-models/vehicle-http.model';
import { VehicleService } from 'src/app/services/vehicle.service';
import { IVehicle } from 'src/app/models/vehicle.model';
import { CustomerService } from 'src/app/services/customer.service';

@Injectable()
export class VehiclesEffects {
    @Effect()
    getVehicle$ = this._actions$.pipe(
        ofType<GetVehicle>(EVehicleActions.GetVehicle),
        map(action => action.payload),
        withLatestFrom(this._store.pipe(select(selectVehicleList))),
        switchMap(([id, vehicles]) => {
            const selectedVehicle = vehicles.filter(vehicle => vehicle.id === id)[0];
            return of(new GetVehicleSuccess(selectedVehicle));
        })
    );

    @Effect()
    getVehicles$ = this._actions$.pipe(
        ofType<GetVehicles>(EVehicleActions.GetVehicles),
        switchMap(() => this._vehicleService.getVehicles()),
        switchMap((vehicles: any) => of(new GetVehiclesSuccess(
            vehicles.map(e => {
                let vehicle = e.payload.toJSON();
                this._customerService.getCustomer(vehicle.customer).subscribe(
                    customer => {
                        vehicle.customer = customer.payload.toJSON();
                        vehicle.customerName = vehicle.customer.name;
                        vehicle.customerAddress = vehicle.customer.address;
                    }
                )
                return vehicle;
            })
        )))
    );

    constructor(
        private _vehicleService: VehicleService,
        private _actions$: Actions,
        private _store: Store<IAppState>,
        private _customerService: CustomerService
    ) { }
}