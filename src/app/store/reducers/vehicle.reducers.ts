import { initialVehicleState, IVehicleState } from '../state/vehicle.state';
import { VehicleActions, EVehicleActions } from '../actions/vehicle.actions';

export const vehicleReducers = (
    state = initialVehicleState,
    action: VehicleActions
): IVehicleState => {
    switch (action.type) {
        case EVehicleActions.GetVehiclesSuccess: {
            return {
                ...state,
                vehicles: action.payload
            }
        }
        case EVehicleActions.GetVehicleSuccess: {
            return {
                ...state,
                selectedVehicle: action.payload
            }
        }

        default:
            return state;
    }
}