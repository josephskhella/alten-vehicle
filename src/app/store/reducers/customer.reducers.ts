import { ECustomerActions, CustomerActions } from '../actions/customer.actions';
import { initialCustomerState, ICustomerState } from '../state/customer.state';

export const customerReducers = (
    state = initialCustomerState,
    action: CustomerActions
): ICustomerState => {
    switch (action.type) {
        case ECustomerActions.GetCustomersSuccess: {
            return {
                ...state,
                customers: action.payload
            }
        }
        case ECustomerActions.GetCustomerSuccess: {
            return {
                ...state,
                selectedCustomer: action.payload
            }
        }

        default:
            return state;
    }
}