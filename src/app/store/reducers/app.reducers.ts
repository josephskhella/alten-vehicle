import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { routerReducer } from '@ngrx/router-store';
import { vehicleReducers } from './vehicle.reducers';
import { customerReducers } from './customer.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
    router: routerReducer,
    vehicles: vehicleReducers,
    customers: customerReducers,
};