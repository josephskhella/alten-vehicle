import { IVehicleState } from "../state/vehicle.state";
import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';

const selectVehicles = (state: IAppState) => state.vehicles;

export const selectVehicleList = createSelector(
    selectVehicles,
    (state: IVehicleState) => state.vehicles
);

export const selectSelectedVehicle = createSelector(
    selectVehicles,
    (state: IVehicleState) => state.selectedVehicle
);

