import { Action } from '@ngrx/store';
import { IVehicle } from 'src/app/models/vehicle.model';

export enum EVehicleActions {
    GetVehicles = '[Vehicle] GetVehicles',
    GetVehiclesSuccess = '[Vehicle] Get Vehicles Success',
    GetVehicle = '[Vehicle] Get Vehicle',
    GetVehicleSuccess = '[Vehicle] Get Vehicle Success'
}

export class GetVehicles implements Action {
    public readonly type = EVehicleActions.GetVehicles;
}
export class GetVehiclesSuccess implements Action {
    public readonly type = EVehicleActions.GetVehiclesSuccess;
    constructor(public payload: IVehicle[]) {}
}
export class GetVehicle implements Action {
    public readonly type = EVehicleActions.GetVehicle;
    constructor(public payload: string) {}
}
export class GetVehicleSuccess implements Action {
    public readonly type = EVehicleActions.GetVehicleSuccess;
    constructor(public payload: IVehicle) {}
}

export type VehicleActions = GetVehicles | GetVehiclesSuccess | GetVehicle | GetVehicleSuccess;