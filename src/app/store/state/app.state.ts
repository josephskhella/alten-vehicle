import { RouterReducerState } from '@ngrx/router-store';
import { IVehicleState, initialVehicleState } from './vehicle.state';
import { ICustomerState, initialCustomerState } from './customer.state';

export interface IAppState {
    router?: RouterReducerState;
    vehicles: IVehicleState;
    customers: ICustomerState;
}

export const initialAppState: IAppState = {
    vehicles: initialVehicleState,
    customers: initialCustomerState,
};

export function getInitialState(): IAppState {
    return initialAppState;
}