import { ICustomer } from 'src/app/models/customer.model';

export interface ICustomerState {
    customers: ICustomer[];
    selectedCustomer: ICustomer;
}

export const initialCustomerState: ICustomerState = {
    customers: null,
    selectedCustomer: null
}