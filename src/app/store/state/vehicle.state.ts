import { IVehicle } from 'src/app/models/vehicle.model';

export interface IVehicleState {
    vehicles: IVehicle[];
    selectedVehicle: IVehicle;
}

export const initialVehicleState: IVehicleState = {
    vehicles: null,
    selectedVehicle: null
}