// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "./assets/data/",
  firebase: {
    apiKey: "AIzaSyCH2mJMrkE5G2kPHWECyJgWe4bAPdH7FtE",
    authDomain: "alten-vehicle.firebaseapp.com",
    databaseURL: "https://alten-vehicle.firebaseio.com",
    projectId: "alten-vehicle",
    storageBucket: "",
    messagingSenderId: "432380718721",
    appId: "1:432380718721:web:f0675a6737b9e95e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
