import { AppPage } from './app.po';
import { browser, logging, element, by, protractor, $$, $ } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  // it('should display welcome message', () => {
  //   page.navigateTo();
  //   expect(page.getTitleText()).toEqual('Welcome to alten-vehicle!');
  // });

  it('should translate to swedish', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    let languageBtn = element(by.css('.language-btn'));
    languageBtn.click();
    browser.sleep(1000);
    let swBtn = element(by.css('.sw-btn'));
    swBtn.click();
    browser.sleep(1000);
    expect(element(by.css('.welcome-msg')).getText()).toContain('Alten Fordonsövervakning');
  });

  it('should open side bar and Home to be visible', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    let menuBtn = element(by.css('.menu-btn'));
    menuBtn.click();
    browser.sleep(1000);
    expect(element(by.css('.home-span')).getText()).toContain('Home');
  });


  it('should translate to swedish then english then swedish', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    let languageBtn = element(by.css('.language-btn'));
    languageBtn.click();
    browser.sleep(1000);
    let swBtn = element(by.css('.sw-btn'));
    swBtn.click();
    browser.sleep(1000);

    languageBtn.click();
    browser.sleep(1000);
    let enBtn = element(by.css('.en-btn'));
    enBtn.click();
    browser.sleep(1000);

    languageBtn.click();
    browser.sleep(1000);
    swBtn.click();
    browser.sleep(1000);
    expect(element(by.css('.footer')).getText()).toContain('Alten Fordonsövervakning');
  });

  it('should filter vehicles data', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    browser.sleep(3000);
    let filterInput = element(by.css('.filter-input'));
    filterInput.sendKeys('y67r675')
    
    browser.sleep(1000);

    expect(element.all(by.css('tbody tr')).count()).toBe(1);
  });

  it('should integrate with backend and get data', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    browser.sleep(6000);

    expect(element.all(by.css('tbody tr')).count()).toBeGreaterThanOrEqual(1);
  });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
